import React from "react";
export default function Die(props) {

  const styles = {
    backgroundColor: props.isHeld ? "#59E391" : "white"
  }

  return (
    <div onClick={props.holdDice} className="die-face animate__animated animate__jello animate__faster" style={styles}>
      <h2 className="die-num animate__animated animate__fadeIn">{props.value}</h2>
    </div>
  )
}
