import React, { useState, useEffect } from "react"
import Die from "./Die"
import { nanoid } from "nanoid";
import Confetti from "react-confetti"
import useWindowSize from "./useWindowsSize"

export default function App() {
  const [diceHolded, setDiceHolded] = useState(false);
  const [error, setError] = useState(false);
  const [dice, setDice] = useState(() => allNewDice());
  const [tenzies, setTenzies] = useState(false);
  const [rollsCount, setRollCount] = useState(0);
  const [time, setTime] = useState(0);


  const [topScores, setTopScores] = useState(
    new Array(10).fill().map(el => ({ id: nanoid() }))
  );

  const { width, height } = useWindowSize()

  console.log({ width, height })




  function getTopScores() {
    const topScores = JSON.parse(localStorage.getItem('topScores'));
    if (topScores) {
      setTopScores(topScores);
    }
  }

  function setTopScoresFn() {
    const newScore = { timeFormatted: formattedTime, rawTime: time, rolls: rollsCount, date: new Date().toLocaleString() };

    const newTopScores = [newScore, ...topScores].sort((a, b) => {
      if (a.rawTime + a.rolls < b.rawTime + b.rolls) {
        return -1;
      } else if (a.rawTime + a.rolls > b.rawTime + b.rolls) {
        return 1;
      }
    }).sort((a, b) => a - b).slice(0, 10);


    setTopScores(newTopScores);
    localStorage.setItem('topScores', JSON.stringify(newTopScores));
  }



  function allNewDice() {
    const newDice = [];
    for (let i = 0; i < 10; i++) {
      newDice.push(generateNewDie())
    }
    return newDice;
  }

  function generateNewDie() {
    return { value: Math.ceil(Math.random() * 6), isHeld: false, id: nanoid() }
  }

  const rollDice = () => {

    if (!tenzies) {
      setDice(oldDice => oldDice.map(die => {
        return die.isHeld ? die : generateNewDie()
      }));
      setRollCount(prev => prev + 1);
    } else {
      setTenzies(false);
      setDice(allNewDice());
      setRollCount(0);
      setTime(0);
      setDiceHolded(false);
    }


  }

  const holdDice = (id) => {
    setDiceHolded(true);
    setDice(oldDice => oldDice.map(die => {
      return die.id === id ? { ...die, isHeld: !die.isHeld } : die
    }))
  }



  useEffect(() => {
    setError(false);
    const allHeld = dice.every(die => die.isHeld);
    const firstValue = dice[0].value;
    const allSameValue = dice.every(die => die.value === firstValue);
    if (allHeld && allSameValue) {
      setTenzies(true);
      setTopScoresFn();
    } else {
      if (diceHolded && allHeld) {
        setError(true);
      }
    }
  }, [dice]);

  useEffect(() => {
    let interval = null;

    if (!tenzies && diceHolded) {
      interval = setInterval(() => {
        setTime((time) => time + 1);
      }, 1000);
    }

    return () => {
      clearInterval(interval)
    };

  }, [tenzies, diceHolded]);

  useEffect(() => {
    getTopScores();
  }, []);


  const minutes = String(Math.floor(time / 60));
  const seconds = String(time % 60);
  const hours = String(Math.floor(minutes / 60));

  const formattedTime = hours.padStart(2, '0') + ':' + minutes.padStart(2, '0') + ':' + seconds.padStart(2, '0');



  const diceElements = dice.map(die => <Die isHeld={die.isHeld} key={die.id} value={die.value} holdDice={() => holdDice(die.id)} />);

  return (
    <React.Fragment>
      <main>
        {tenzies && <Confetti width={width}
          height={height} />}
        <section className="container">
          <h1 className="title">Tenzies</h1>
          <p className="instructions">Roll until all dice are the same. Click each die to freeze it at its current value between rolls.</p>
          <div className="dice-container">
            {diceElements}
          </div>
          <div className="stats">
            {
              <div>Ellapsed time: {formattedTime}</div>
            }
            {<div># of Rolls: {rollsCount}</div>}
          </div>
          {tenzies && <h2 className="win-message">Congratulations! You Win!</h2>}
          {error && <h2 className="error">Error! You must hold all dice with same value!</h2>}
          {!error && <button className="roll-dice" onClick={rollDice}>
            {tenzies ? "Start New Game" : "Roll"}
          </button>}

        </section>

        <section className="container-top-scores">
          <h2>Top Scores:</h2>
          <h4>Scores with lowest time and dice rolls registered</h4>
          <h5>(In local storage)</h5>

          <ul>
            {topScores.map((score, i) => {
              return (
                score.rolls &&
                  (<li key={i} style={{ marginBottom: '5px' }}>
                    <div className="score">
                      <div className="score-number">
                        <div>#{i + 1}</div>
                      </div>
                      <div className="score-detail">
                        <span>Time: </span>
                        <span>{score.timeFormatted}</span>
                      </div>
                      <div className="score-detail">
                        <span>Rolls: </span>
                        <span>{score.rolls}</span>
                      </div>
                      <div className="score-detail">
                        <span>Date: </span>
                        <span>{score.date}</span>
                      </div>
                    </div>
                  </li>)
              )
            })
            }
          </ul>

        </section>

      </main>



    </React.Fragment>
  )
}
